# wang-nw-builder-example

```
"scripts": {
  "build:win": "nwBuilder",
  "build:linux": "nwBuilder --platform=linux",
  "build:osx": "nwBuilder --platform=osx",
  "build:script": "ts-node build.ts",
  "build:script1": "node build1.mjs",
  "build:script2": "node build2.js"
}
```

- 命令行方式打包:

`build:win` 、 `build:linux` 、 `build:osx`

- 脚本方式打包:

`build:script` 、 `build:script1` 、 `build:script2`

源码地址: https://gitee.com/consolelog/wang-nw-builder.git
